#!/bin/bash

PROJECTROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source settings.sh

mkdir $PROJECTROOT/bin 2> /dev/null

echo -e "\e[32mCleaning...\e[39m [output dir $PROJECTROOT/bin]"
. build/clean.sh

echo -e "\e[32mBuilding... \e[39m[src dir $PROJECTROOT/src]"
SRCFILES="$( find src -name *.c* )"
for sf in $SRCFILES
do
	. build/cc.sh $sf
done

echo -e "\e[32mLinking... \e[39m[output file $PROJECTROOT/bin/$OUTPUTNAME]"
. build/link.sh

echo -e "\e[34mDone.\e[39m"
