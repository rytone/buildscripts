#!/bin/bash

if rm $PROJECTROOT/bin/obj/* 2> /dev/null; then echo -e "\e[35mRemoved objects\e[39m"; fi
if rm $PROJECTROOT/bin/$OUTPUTNAME 2> /dev/null; then echo -e "\e[35mRemoved linked binary\e[39m"; fi
