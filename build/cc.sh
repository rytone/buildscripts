#!/bin/bash

echo -e "\e[35mCC $1\e[39m"
NODIR=$(basename "$1")
FILENAME="${NODIR%.*}"
$CC $PROJECTROOT/$1 -I$PROJECTROOT/include $CCFLAGS -c -o $PROJECTROOT/bin/obj/$FILENAME.obj
